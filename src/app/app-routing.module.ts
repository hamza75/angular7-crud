import { ProductsComponent } from "./components/products/products.component";
import { ProductDetailComponent } from "./components/product-detail/product-detail.component";
import { ProductEditComponent } from "./components/product-edit/product-edit.component";
import { ProductAddComponent } from "./components/product-add/product-add.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "product",
    component: ProductsComponent,
    data: { title: "Liste of Products" }
  },
  {
    path: "product-details",
    component: ProductDetailComponent,
    // tslint:disable-next-line: quotemark
    data: { title: "Product details" }
  },
  {
    path: "product-add",
    component: ProductAddComponent,
    data: { title: "Add new Product" }
  },
  {
    path: "product-edit/:id",
    component: ProductEditComponent,
    data: { title: "Edit Product" }
  },
  {
    path: "",
    redirectTo: "/products",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
